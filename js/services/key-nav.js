angular.module('keyNav', []).service('keyNav', function () {
    var keys = {
        38: 'up',
        40: 'down',
        37: 'left',
        39: 'right'
    };
    var callbacks = {
        up: [],
        down: [],
        left: [],
        right: []
    };

    function runCallbacks (action, event) {
        for (var i in callbacks[action]) {
            callbacks[action][i](event);
        }
    }

    function keyListener (event) {
        var action = keys[event.keyCode];
        if (action) {
            runCallbacks(action, event);
        }
    }

    this.bind = function  (action, callback) {
        if (callbacks[action]) {
            callbacks[action].push(callback);
        }
    };

    window.onkeydown = keyListener;

    return this;
});