angular.module('tvApp').directive('cinemaHalls', function () {
    return {
        restrict: 'E',
        scope: true,
        templateUrl: 'views/partials/cinema-halls.html',
        controller: function ($scope, $http, keyNav) {

            var jSliders = $('cinema-slider');

            var jCinemaHalls = $('.cinema-halls');

            var titleHeight = 45;
            var minTopOffset = 45;

            $scope.focusedSlider = 0;

            function changeRowFocus (newFocus) {

                if (newFocus < 0) {
                    newFocus = 0;
                }
                if (newFocus > jSliders.length - 1) {
                    newFocus = jSliders.length - 1;
                }

                jSliders.attr('focused', '');
                jSliders.eq(newFocus).attr('focused', 'focused');

                var marginTop = minTopOffset - titleHeight * newFocus;
                jCinemaHalls.css('margin-top', marginTop);

                $scope.focusedSlider = newFocus;

            }

            changeRowFocus(0);

            keyNav.bind('up', function (event) {
                changeRowFocus($scope.focusedSlider - 1);
                event.preventDefault();
                $scope.$apply();
            });
            keyNav.bind('down', function (event) {
                changeRowFocus($scope.focusedSlider + 1);
                event.preventDefault();
                $scope.$apply();
            });

        }
    }
});