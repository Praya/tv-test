moment.locale('ru');

var tvApp = angular.module('tvApp', ['ngRoute', 'angularMoment', 'keyNav']);

tvApp.config(function ($routeProvider, $locationProvider) {

    $locationProvider.html5Mode(true);

    $routeProvider.when('/cinema-halls', {
        templateUrl: 'views/cinema-halls-page.html'
    }).otherwise({
        redirectTo: '/cinema-halls'
    });

});